import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Ketik kata yang ingin Anda periksa");
        String word = in.nextLine();

        if (truePalindrome(word))
            System.out.println("Ya, ini adalah sebuah palindrome");
        else
            System.out.println("Tidak, ini bukan sebuah palindrome");
    }

    private static boolean palindromeChecker(String word, int firstLetter, int lastLetter) {
        if (firstLetter == lastLetter)
            return true;

        if ((word.charAt(firstLetter)) != (word.charAt(lastLetter)))
            return false;

        if (firstLetter < lastLetter + 1)
            return palindromeChecker(word, firstLetter + 1, lastLetter - 1);

        return true;

    }

    private static boolean truePalindrome(String word){
        int x = word.length();

        if (x == 0)
            return true;

        return palindromeChecker(word, 0, x - 1);

    }
}
